package com.example.android_driver_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class ReportDeliveryProblem extends AppCompatActivity {

    Button btnSubmit;
    EditText reportEditText;
    int delivery_id;
    private String url = "https://gaagaa-backend.herokuapp.com";
    String reportText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_delivery_problem);

        delivery_id = getIntent().getIntExtra("delivery_id", 0);

        //view elements declaration
        btnSubmit = (Button)findViewById(R.id.btnSubmit);
        reportEditText = (EditText)findViewById(R.id.reportEditText);

        //view elements functionality
        btnSubmit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                reportText = reportEditText.getText().toString();
                try{
                    postFail();
                } catch (Exception e){
                    System.out.println(e);
                }
                Intent intent = new Intent(ReportDeliveryProblem.this, ReportSubmitted.class);
                startActivity(intent);
            }
        });
    }

    private Boolean postFail() throws Exception{
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        String json_str = reportText.replace("\n", "\\n");;
        String bodyStr = "{\n" +
                "  \"parcel_id\": " + String.valueOf(delivery_id) +",\n" +
                "  \"status\": \"Fail\",\n" +
                "  \"report\": \""+ json_str + "\"\n" +
                "}\n";
        RequestBody body = RequestBody.create(JSON, bodyStr);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
        Request request = new Request.Builder()
                .url(url + "/driver/update_status")
                .addHeader("Authorization", "Bearer " + LoginActivity.token)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        System.out.println(response.code() == 200);
        return response.code() == 200;
    }
}
