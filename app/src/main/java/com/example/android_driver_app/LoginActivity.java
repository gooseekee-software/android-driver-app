package com.example.android_driver_app;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class LoginActivity extends AppCompatActivity {

    /**
     * Elements of the view that will be used
     */
    private EditText Username; //Edit text field to input user's username
    private EditText Password; //Edit text field to input user's password
    private Button Login; //button to be pressed after filling in above fields
    private TextView InvalidInfo; //this text will be shown in case of incorrect input

    private String url = "https://gaagaa-backend.herokuapp.com";
    static String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //assigning the elements
        Username = (EditText)findViewById(R.id.loginET);
        Password = (EditText)findViewById(R.id.passwordET);
        Login = (Button)findViewById(R.id.login);
        InvalidInfo = (TextView)findViewById(R.id.tvInvalid);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    validate(Username.getText().toString(), Password.getText().toString());
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        });
    }

    /**
     * This method is used to validate user's input: username and password
     * @param userName
     * @param userPassword
     */
    private void validate(String userName, String userPassword) throws Exception {
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        String bodyStr = "{\n" +
                "  \"login\": \"" + String.valueOf(userName) + "\",\n" +
                "  \"password\": \"" + String.valueOf(userPassword) + "\"\n" +
                "}\n";
        RequestBody body = RequestBody.create(JSON, bodyStr);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
        Request request = new Request.Builder()
                .url(url + "/driver/login")
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        if (response.code() == 200) {
            String str = response.body().string();
            JSONObject jsonobj = new JSONObject(str);
            token = jsonobj.getString("access_token");
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }else {
            InvalidInfo.setVisibility(View.VISIBLE);
        }
    }
}
