package com.example.android_driver_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;

public class CurrDelivActivity extends AppCompatActivity {

    private Button btnCompleted;
    private Button btnProblem;
    private TextView destinationAddress;
    private TextView pickupAddress;
    private TextView avTime;
    private Button btnContact;
    private Button btnNavigation;

    private String clientName;
    private String clientPhone;
    private String clientEmail;
    private double destLat;
    private double destLong;
    private int delivery_id;

    private String url = "https://gaagaa-backend.herokuapp.com";


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_curr_deliv);

        //view elements declaration
        btnCompleted = (Button)findViewById(R.id.btnCompleted);
        btnProblem = (Button)findViewById(R.id.btnProblem);
        destinationAddress = (TextView)findViewById(R.id.destinationAddressVIEW);
        pickupAddress = (TextView)findViewById(R.id.pickupAddressVIEW);
        avTime = (TextView)findViewById(R.id.avTimeVIEW);
        btnContact = (Button)findViewById(R.id.btnContact);
        btnNavigation = (Button)findViewById(R.id.btnNavigation);

        try{
            getInfo();
        } catch (Exception e){}

        //view elements functionality
        btnNavigation.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CurrDelivActivity.this, NavigationActivity.class);
                intent.putExtra("Lat", destLat);
                intent.putExtra("Long", destLong);
                startActivity(intent);
            }
        });
        btnContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CurrDelivActivity.this, ClientContacts.class);
                intent.putExtra("Name", clientName);
                intent.putExtra("Phone", clientPhone);
                intent.putExtra("Mail", clientEmail);
                startActivity(intent);
            }
        });
        btnCompleted.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                try{
                    postSuccess();
                } catch (Exception e) {
                    System.out.println(e);
                }
                Intent intent = new Intent(CurrDelivActivity.this, CompletedDeliv.class);
                startActivity(intent);
            }
        });
        btnProblem.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CurrDelivActivity.this, ReportDeliveryProblem.class);
                intent.putExtra("delivery_id", delivery_id);
                startActivity(intent);
            }
        });

    }

    private void getInfo() throws JSONException {

        JSONObject jsonObj = new JSONObject(getIntent().getStringExtra("JSONobjString"));
        delivery_id = jsonObj.getInt("delivery_id");
        String destAddr = jsonObj.getString("delivery_address");
        destinationAddress.setText(destAddr);
        String pickupAddr = jsonObj.getString("pickup_address");
        pickupAddress.setText(pickupAddr);
        String time = jsonObj.getString("time_windows");
        avTime.setText(time);
        clientName = jsonObj.getString("client_name");
        clientPhone = jsonObj.getString("telephone");
        clientEmail = jsonObj.getString("email");
        JSONObject destCoord = jsonObj.getJSONObject("delivery_coordinates");
        destLat = destCoord.getDouble("latitude");
        destLong = destCoord.getDouble("longitude");
    }

    private Boolean postSuccess() throws Exception{
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        String bodyStr = "{\n" +
                "  \"parcel_id\": " + String.valueOf(delivery_id) +",\n" +
                "  \"status\": \"Success\"" +
                "}\n";
        RequestBody body = RequestBody.create(JSON, bodyStr);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
        Request request = new Request.Builder()
                .url(url + "/driver/update_status")
                .addHeader("Authorization", "Bearer " + LoginActivity.token)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        System.out.println(response.code() == 200);
        return response.code() == 200;
    }


}
