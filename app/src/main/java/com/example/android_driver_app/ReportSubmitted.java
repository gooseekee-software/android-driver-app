package com.example.android_driver_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ReportSubmitted extends AppCompatActivity {

    private Button backToMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_submitted);

        //view elements declaration
        backToMain = (Button)findViewById(R.id.backToMain);

        //view elements functionality
        backToMain.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ReportSubmitted.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
