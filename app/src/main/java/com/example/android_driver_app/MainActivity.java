package com.example.android_driver_app;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Looper;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.google.android.gms.location.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;


public class MainActivity extends AppCompatActivity {

    private Button currDeliv;
    private Button histDeliv;
    private Button schedDeliv;

    private LocationRequest mLocationRequest;
    static Location imHere;

    private long UPDATE_INTERVAL = 30 * 1000;  /* 30 secs */
    private long FASTEST_INTERVAL = 20 * 1000; /* 20 sec */

    private String url = "https://gaagaa-backend.herokuapp.com";
    private int driver_id = 14;
    private JSONObject currDelivery;
    private JSONArray histDeliveries;
    private JSONArray schedDeliveries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startLocationUpdates();
        getLastLocation();

        //view elements declaration
        currDeliv = (Button)findViewById(R.id.currDeliv);
        schedDeliv = (Button)findViewById(R.id.schedDeliv);
        histDeliv = (Button)findViewById(R.id.histDeliv);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            currDelivery = getCurrDeliv();
            if (currDelivery == null){
                currDeliv.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, NoCurrDelivActivity.class);
                        intent.putExtra("message", "NO CURRENT\nDELIVERIES");
                        startActivity(intent);
                    }
                });
            } else {
                currDeliv.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, CurrDelivActivity.class);
                        intent.putExtra("JSONobjString", currDelivery.toString());
                        startActivity(intent);
                    }
                });
            }

            histDeliveries = getHistDeliv();
            if (histDeliveries == null){
                histDeliv.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, NoCurrDelivActivity.class);
                        intent.putExtra("message", "NO PREVIOUS\nDELIVERIES");
                        startActivity(intent);
                    }
                });
            } else {
                histDeliv.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, HistoryDeliveries.class);
                        intent.putExtra("array", histDeliveries.toString());
                        startActivity(intent);
                    }
                });
            }

            schedDeliveries = getFutureDeliv();
            if (schedDeliveries == null){
                schedDeliv.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, NoCurrDelivActivity.class);
                        intent.putExtra("message", "NO SCHEDULED\nDELIVERIES");
                        startActivity(intent);
                    }
                });
            } else {
                schedDeliv.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, ScheduledDeliveries.class);
                        intent.putExtra("array", schedDeliveries.toString());
                        startActivity(intent);
                    }
                });
            }
        } catch (Exception e){
            System.out.println(e);
        }
    }

    private JSONObject getCurrDeliv() throws Exception {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        //OkHttpClient client = new OkHttpClient();
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
        Request.Builder builder = new Request.Builder();
        builder.addHeader("Authorization", "Bearer " + LoginActivity.token);
        builder.url(url + "/driver/current_delivery");
        Request request = builder.build();
        Response response = client.newCall(request).execute();
        if(response.code() == 200) {
            String str = response.body().string();
            JSONObject jsonobj = new JSONObject(str);
            if(jsonobj.getString("message").equals("No current deliveries")){
                return null;
            } else {
                return jsonobj.getJSONObject("delivery");
            }
        } else {
            throw new Exception(response.toString());
        }
    }

    private JSONArray getHistDeliv() throws Exception {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
        Request.Builder builder = new Request.Builder();
        builder.addHeader("Authorization", "Bearer " + LoginActivity.token);
        builder.url(url + "/driver/passed_deliveries");
        Request request = builder.build();
        Response response = client.newCall(request).execute();
        if(response.code() == 200) {
            String str = response.body().string();
            JSONObject jsonobj = new JSONObject(str);
            if(jsonobj.getString("message").equals("No previous deliveries deliveries")){
                return null;
            } else {
                JSONArray jsonarr = jsonobj.getJSONArray("delivery");
                return jsonarr;
            }
        } else {
            throw new Exception(response.toString());
        }
    }

    private JSONArray getFutureDeliv() throws Exception {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
        Request.Builder builder = new Request.Builder();
        builder.addHeader("Authorization", "Bearer " + LoginActivity.token);
        builder.url(url + "/driver/future_deliveries");
        Request request = builder.build();
        Response response = client.newCall(request).execute();
        if(response.code() == 200) {
            String str = response.body().string();
            JSONObject jsonobj = new JSONObject(str);
            if(jsonobj.getString("message").equals("No more deliveries")){
                return null;
            } else {
                JSONArray jsonarr = jsonobj.getJSONArray("delivery");
                return jsonarr;
            }
        } else {
            throw new Exception(response.toString());
        }
    }

    private Boolean postCoordinates (double Lat, double Long) throws Exception{
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        String bodyStr = "{\n" +
                "  \"driver_id\": " + String.valueOf(driver_id) +",\n" +
                "  \"latitude\": \"" + String.valueOf(Lat) + "\",\n" +
                "  \"longitude\": \""+ String.valueOf(Long) + "\"\n" +
                "}\n";
        RequestBody body = RequestBody.create(JSON, bodyStr);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
        Request request = new Request.Builder()
                .url(url + "/driver/map")
                .addHeader("Authorization", "Bearer " + LoginActivity.token)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        System.out.println(response.code() == 200);
        return response.code() == 200;
    }

    protected void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        PackageManager pm = getPackageManager();
        int hasPerm = pm.checkPermission(android.Manifest.permission.ACCESS_FINE_LOCATION, getPackageName());
        if (hasPerm == -1) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                    123);
        }

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        onLocationChanged(locationResult.getLastLocation());
                    }
                },
                Looper.myLooper());
    }

    public void onLocationChanged(Location location) {
        // New location has now been determined
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        imHere = location;
        try{
            postCoordinates(imHere.getLatitude(), imHere.getLongitude());
        } catch (Exception e){
            System.out.println(e);
        }
        System.out.println("ONLOCATIONCHANGED!!!!!!!!");
    }

    public void getLastLocation() {
        // Get last known recent location using new Google Play Services SDK (v11+)
        FusedLocationProviderClient locationClient = getFusedLocationProviderClient(this);

        PackageManager pm = getPackageManager();
        int hasPerm = pm.checkPermission(android.Manifest.permission.ACCESS_FINE_LOCATION, getPackageName());
        if (hasPerm == -1) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                    123);
        }
        locationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // GPS location can be null if GPS is switched off
                        if (location != null) {
                            onLocationChanged(location);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("MapDemoActivity", "Error trying to get last GPS location");
                        e.printStackTrace();
                    }
                });
    }
}
