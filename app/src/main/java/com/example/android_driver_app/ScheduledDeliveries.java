package com.example.android_driver_app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;

public class ScheduledDeliveries extends AppCompatActivity {

    private RecyclerView rvDeliveries;

    private LinkedList<JSONObject> deliveries;
    private JSONArray array;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_deliveries);
        try{
            array = new JSONArray(getIntent().getStringExtra("array"));
        } catch (Exception e){}
        rvDeliveries = (RecyclerView)findViewById(R.id.rvDeliveries); //view that will hold report cards
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rvDeliveries.setLayoutManager(llm);
        rvDeliveries.setHasFixedSize(true);
        rvDeliveries.setAdapter(new RVAdapter(array));
    }
}
